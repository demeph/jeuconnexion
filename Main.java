import java.awt.Color;
import java.awt.Polygon;
import java.awt.event.*;
import java.util.Vector;
import java.util.Stack;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class Main implements ActionListener
{
	final Plateau plateau_;
	public Joueur J1, J2;
	static Joueur joueurCourant;
	static int peutModif = 0;
	static int J1ouJ2 = 1;
    static int nbEtoile = 3;
	static boolean gagne_ = false;
	private JMenuItem item1 = new JMenuItem("Le Joueur Rouge"); // Les affichages dans le menu
	private JMenuItem item2 = new JMenuItem("Le Joueur Bleu");
	private JMenuItem item3 = new JMenuItem("Le Joueur Rouge");
	private JMenuItem item4 = new JMenuItem("Le Joueur Bleu");
	private JMenuItem item5 = new JMenuItem("Abandonner");
	private JMenuItem item6 = new JMenuItem("Recommencer");
	private JMenuItem item7 = new JMenuItem("Existence des chemins-cases");
	private JMenuItem item8 = new JMenuItem("Nombre d'étoiles");
	private JMenuItem item9 = new JMenuItem("Afficher le score");
	private JMenuItem item10 = new JMenuItem("Relier les cases au minimum");
    private boolean premierConnecte;
    private Stack<Joueur> premierEtoileConnecte;


	public Main()
	{
		this.J1 = new Joueur("Le Joueur Rouge",Color.red);
      	this.J2 = new Joueur("Le Joueur Bleu",Color.cyan);
		this.plateau_ = new Plateau();
		this.plateau_.genererEtoiles(J1,nbEtoile);
		this.plateau_.genererEtoiles(J2,nbEtoile);
        plateau_.setNbcaseTotal(plateau_.getNbcaseTotal()-2*nbEtoile);
        premierConnecte = false;
        premierEtoileConnecte = new Stack<Joueur>();
		JFrame win = new JFrame();
		JMenu menu1 = new JMenu("Fonctionnalités");
		JMenu submenu1 = new JMenu("Afficher les composantes");
		JMenu submenu2 = new JMenu("Relier les composantes");
		menu1.add(submenu1);
		submenu1.add(item1);
		submenu1.add(item2);
		menu1.add(submenu2);
		submenu2.add(item3);
		submenu2.add(item4);
		menu1.add(item7);
		menu1.add(item8);
		menu1.add(item9);
		menu1.add(item10);
		
		item1.addActionListener(this);// pour que ca soit clickable
		item2.addActionListener(this);
		item3.addActionListener(this);
		item4.addActionListener(this);
		item5.addActionListener(this);
		item6.addActionListener(this);
		item7.addActionListener(this);
		item8.addActionListener(this);
        item9.addActionListener(this);
        item10.addActionListener(this);
		JPanel jp = (JPanel) win.getContentPane();	
		JMenuBar barreMenu = new JMenuBar();
		barreMenu.add(menu1);
		barreMenu.add(item6);
		barreMenu.add(item5);
		
		jp.add(plateau_);// Ajoute le composant à la fenêtre
		win.setJMenuBar(barreMenu);
		win.setTitle("Jeu Connexion");
		win.setResizable(false);
		win.setSize(420, 450);// Dimensionne la fenetre
		win.setVisible(true);// Affiche la fenetre
        win.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	/*
	 * Retourne vrai s'il existe un chemin d'une couleur donnée entre deux cases
	 * */
	public boolean existeCheminCases(Case c1,Case c2){
		if(!(c1.caseVide())&&!(c2.caseVide())&&(c1.getColor()== c2.getColor())){
			if(c1.getColor()==Color.red){
				return J1.getUC().estConnecte(c1.getPion(), c2.getPion());
			} else return J2.getUC().estConnecte(c1.getPion(), c2.getPion());
		} else return false;
	}

	public void joueDeuxHumains()
	{
		plateau_.addMouseMotionListener(new MouseMotionListener(){
			Polygon p;
			@Override
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
				p=plateau_.getPol();
				if(!p.contains(arg0.getPoint())) {  plateau_.repaint();  }
			}
		});// Repeint plateau_ lorsque la souris se déplace

		final int nbCase = plateau_.getnbCase();

		plateau_.addMouseListener(new MouseAdapter(){
			Polygon carreGlobal;
			int c, l;
			@Override
			public void mouseClicked(MouseEvent arg0)
            {
				carreGlobal = plateau_.getCarre();
				l = plateau_.getNumero()/plateau_.getnbCase();
				c = plateau_.getNumero()%plateau_.getnbCase();
				super.mouseClicked(arg0);
				if (peutModif == 0)
                {
                   // on verifie si l'endroi on click appartien au getCarre() global
					if (!(gagne_)) {
						if (carreGlobal.contains(arg0.getPoint())) {
							if (J1ouJ2 == 1) {
								if (plateau_.estDispo(l, c)) {
									joueurCourant = J1;
									Pion pion = new Pion(l, c, J1.getColor());
									plateau_.ajouterPion(J1, pion);
									plateau_.setNbcaseTotal(plateau_.getNbcaseTotal()-1);
									J1ouJ2 = 0;
                                    if (!(premierConnecte) && (getNbEtoilesConnectes(J1)!=0))
                                    {
                                        premierConnecte = true;
                                        premierEtoileConnecte.push(J1);
                                    }
								}
							} else {
								if (plateau_.estDispo(l, c)) {
									joueurCourant = J2;
									Pion pion = new Pion(l, c, J2.getColor());
									plateau_.ajouterPion(J2, pion);
									plateau_.setNbcaseTotal(plateau_.getNbcaseTotal()-1);
									J1ouJ2 = 1;
                                    if (!(premierConnecte) && (getNbEtoilesConnectes(J2)!=0))
                                    {
                                        premierConnecte = true;
                                        premierEtoileConnecte.push(J2);
                                    }
								}
							}
						}
					}
                    existeGagnant();
                }
			}
		});// Evenement qui survient au clicque
	}

    public void existeGagnant() {
        if (aGagne(J1))
        {
            gagne_ = true;
            JOptionPane.showMessageDialog(null,"Le Joueur Rouge a gagné !");
        } else if (aGagne(J2))
        {
            gagne_ = true;
            JOptionPane.showMessageDialog(null,"Le Joueur Bleu a gagné !");
        }
        // ici je verifie s'il existe un chemin entre les etoiles
        if (!(cheminEtoiles(J1)) || (!(cheminEtoiles(J2)))) {
            if (J1.getUC().getSize() > J2.getUC().getSize())
            {
                gagne_ = true; //Si l'etoile est entourée par l'adversaire on arrete le jeu
                JOptionPane.showMessageDialog(null, "Le jeu est terminé !");
                JOptionPane.showMessageDialog(null, "Le gagnant est : " + J1.toString());//Celui qui a relié l'etoile plus rapide a gagné
            }
            else if (J1.getUC().getSize() < J2.getUC().getSize())
            {
                gagne_ = true;
                JOptionPane.showMessageDialog(null, "Le jeu est terminé !");
                JOptionPane.showMessageDialog(null, "Le gagnant est : " + J2.toString());
            }
            else
            {
                gagne_ = true;
                JOptionPane.showMessageDialog(null, "Le jeu est terminé !");
                if (premierConnecte) {
                    JOptionPane.showMessageDialog(null, "Le gagnant est : " + premierEtoileConnecte.pop().toString());
                }
                else {
                    JOptionPane.showMessageDialog(null, "Match nul !");// Si celui qui a pas relié l'étoile donc match nul
                }
            }
        }
    }

    public void afficherComposante()
	{
		final int nbCase = plateau_.getnbCase();
		plateau_.addMouseListener(new MouseAdapter(){
			Polygon carreGlobal;
			int c, l;
			boolean estSelected = false;
			@Override
			public void mouseClicked(MouseEvent arg0) {
				carreGlobal = plateau_.getCarre();
				l = plateau_.getNumero()/plateau_.getnbCase();
				c = plateau_.getNumero()%plateau_.getnbCase();
				String afficher ="";
				super.mouseClicked(arg0);
				if  (peutModif == 1 )
				{
					do {
						if (carreGlobal.contains(arg0.getPoint()))
						{
							estSelected = true;
							Pion pion = plateau_.getPion(l, c);
							if ( !(plateau_.estDispo(l,c)))
							{
                                if (joueurCourant.getColor() == pion.getColor()) {
                                    afficher = plateau_.afficherComposante(joueurCourant, pion);
                                }
								JOptionPane.showMessageDialog(null,afficher);

							}
						}
					} while (estSelected == false ) ;
					peutModif = 0;
				}
			}
		});// Evenement qui survient au clic
	}

	public int nombreEtoiles(Joueur j,Case p)
	{
		Vector<Case> lesEtoiles = j.getEtoiles();
		int nbEtoiles = 0;
		for (int i = 0;i < lesEtoiles.size() ;++i ) {
			if (j.getUC().estConnecte(p.getPion(),lesEtoiles.get(i).getPion()))
			{
				++nbEtoiles;
			}
		}
		return nbEtoiles;
	}

	public void nombreEtoiles()
	{
		final int nbCase = plateau_.getnbCase();
		plateau_.addMouseListener(new MouseAdapter(){
			Polygon carreGlobal;
			int c, l;
			boolean estSelected = false;
			@Override
			public void mouseClicked(MouseEvent arg0) {
				carreGlobal = plateau_.getCarre();
				l = plateau_.getNumero()/plateau_.getnbCase();
				c = plateau_.getNumero()%plateau_.getnbCase();
				String afficher ="";
				super.mouseClicked(arg0);
				if  (peutModif == 3 )
				{
					do {
						if (carreGlobal.contains(arg0.getPoint()))
						{
							estSelected = true;
							Case pion = plateau_.getCase(l, c);
							if (pion.getColor() == Color.RED)
							{
								afficher = "Le Joueur Rouge possède : " + nombreEtoiles(J1,pion);
								JOptionPane.showMessageDialog(null,afficher);
							} else if (pion.getColor() == Color.cyan)
							{
								afficher = "Le Joueur Bleu possède : " + nombreEtoiles(J2,pion);
								JOptionPane.showMessageDialog(null,afficher);
							}
							else
							{
								afficher = "Cette case ne correspond aucune case colorié !";
								JOptionPane.showMessageDialog(null,afficher);
							}
						}
					} while (estSelected == false ) ;
					peutModif = 0;
				}
			}
		});// Evenement qui survient au clic
	}

	public void relieComposantes()
	{
		final int nbCase = plateau_.getnbCase();
		plateau_.addMouseListener(new MouseAdapter(){
			Polygon carreGlobal;
			int c, l;
			boolean estSelected = false;
			@Override
			public void mouseClicked(MouseEvent arg0) {
				carreGlobal = plateau_.getCarre();
				l = plateau_.getNumero()/plateau_.getnbCase();
				c = plateau_.getNumero()%plateau_.getnbCase();
				super.mouseClicked(arg0);
				if  (peutModif == 2 )
				{
					do {
						if (carreGlobal.contains(arg0.getPoint()))
						{
							estSelected = true;
							Pion pion = new Pion(l,c,joueurCourant.getColor());
							if ( (plateau_.estDispo(l,c)))	{
								if (plateau_.relieComposantes(joueurCourant,pion)){
								    JOptionPane.showMessageDialog(null,"Oui, "+ pion.toString()+" : relie deux composantes après la coloration.");
                                }
                                else
                                {
									JOptionPane.showMessageDialog(null,"Non, "+ pion.toString()+" : ne relie deux composantes après la coloration.");
								}									
							} else
                            {
                                JOptionPane.showMessageDialog(null,"Cette case est déjà colorié.");
                            }
						}						
					} while ( estSelected != true);
					peutModif = 0;
				} 
			}
		});// Evenement qui survient au clic
	}

	public String afficheScores()
	{
		int nbEtoilesConnectesJ1 = 0;
        int nbEtoilesConnectesJ2 =0;
		String message = "";
		nbEtoilesConnectesJ1 = getNbEtoilesConnectes(J1);
		nbEtoilesConnectesJ2 = getNbEtoilesConnectes(J2);
		message += "Rouge = " + nbEtoilesConnectesJ1;
        message += "\nBleu = " + nbEtoilesConnectesJ2;
        return message;
	}

	public int getNbEtoilesConnectes(Joueur joueur_) {
		int nbEtoilesConnectes= 0;
		Vector<Case> lesetoiles = joueur_.getEtoiles();
		for (int i = 0; i < lesetoiles.size();++i)
        {
            Case case1 = lesetoiles.get(i);
            for (int j = i+1; j < lesetoiles.size();++j)
            {
                Case case2 = lesetoiles.get(j);
                if (existeCheminCases(case1,case2))
                {
                    ++nbEtoilesConnectes;
                }
            }
        }
		return nbEtoilesConnectes;
	}

    public int existeCheminEtoiles(Joueur joueur_) {
        int nbEtoilesConnectes= 0;
        Vector<Case> lesetoiles = joueur_.getEtoiles();
        for (int i = 0; i < lesetoiles.size();++i)
        {
            Case case1 = lesetoiles.get(i);
            for (int j = i+1; j < lesetoiles.size();++j)
            {
                Case case2 = lesetoiles.get(j);
                if (plateau_.relieCaseMin(case1,case2).size()!= 0)
                {
                    ++nbEtoilesConnectes;
                }
            }
        }
        return nbEtoilesConnectes;
    }

    public int sommeEntier(int nbEtoile)
    {
        int nb = 0;
        for (int i = 0; i < nbEtoile-1;++i)
        {
            nb += (nbEtoile-1) -i;
        }
        return nb;
    }

	public boolean aGagne(Joueur j)
	{
		int nbEtoilesConnecte = getNbEtoilesConnectes(j);
		int nbEtoile = j.nombreEtoiles();
		return  sommeEntier(nbEtoile) == nbEtoilesConnecte;
	}

    public boolean cheminEtoiles(Joueur j)
    {
        int nbEtoilesConnecte = existeCheminEtoiles(j);
        int nbEtoile = j.nombreEtoiles();
        return  sommeEntier(nbEtoile) == nbEtoilesConnecte;
    }

	@SuppressWarnings("static-access")// Accessible au menu : Abandonner ou etc
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == item1) {
			peutModif = 1;
			joueurCourant = J1;
			afficherComposante();
		} else if (e.getSource() == item2) {
			peutModif = 1;
			joueurCourant = J2;
			afficherComposante();
		} else if (e.getSource() == item3) {
			joueurCourant = J1;
			peutModif = 2;
			relieComposantes();
		} else if (e.getSource() == item4) {
			joueurCourant = J2;
			peutModif = 2;
			relieComposantes();
		} else if (e.getSource() == item5) {
			JOptionPane jop = new JOptionPane();
			int option = jop.showConfirmDialog(null,
					"Voulez-vous abandonner le jeu ?",
					null, JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (option == JOptionPane.OK_OPTION) {
				if (joueurCourant != J1) {
					JOptionPane.showMessageDialog(null, J2.getNom() + " a gagné !");
				} else {
					JOptionPane.showMessageDialog(null, J2.getNom() + " a gagné !");
				}
			}
		} else if (e.getSource() == item6) {
			JOptionPane restart = new JOptionPane();
			int option = restart.showConfirmDialog(null,
					"Voulez-vous recommencer le jeu ?",
					null, JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (option == JOptionPane.OK_OPTION) {
				plateau_.reinitialiser();
                plateau_.setNbcaseTotal(plateau_.getNbcaseTotal()-2*nbEtoile);
				plateau_.repaint();
				//peut etre on demandera combien etoile on voudrait afficher pour le nouvel jeu
				J1.getUC().getVector().clear();
				J2.getUC().getVector().clear();
                J1.getEtoiles().clear();
                J2.getEtoiles().clear();
                premierConnecte = false;
                premierEtoileConnecte.removeAllElements();
                plateau_.genererEtoiles(J1,nbEtoile);
                plateau_.genererEtoiles(J2,nbEtoile);
                J1ouJ2 = 1;
                gagne_ = false;
			}
		} else if (e.getSource() == item7) { //Utile pour demander les fonctionnalités
			int l1 = 0, c1 = 0;
			int l2 = 0, c2 = 0;

			JTextField l1Field = new JTextField(5);
			JTextField c1Field = new JTextField(5);
			JPanel case1 = new JPanel();
			case1.add(new JLabel("Ligne :"));
			case1.add(l1Field);
			case1.add(new JLabel("Colonne :"));
			case1.add(c1Field);

			JTextField l2Field = new JTextField(5);
			JTextField c2Field = new JTextField(5);
			JPanel case2 = new JPanel();
			case2.add(new JLabel("Ligne :"));
			case2.add(l2Field);
			case2.add(new JLabel("Colonne :"));
			case2.add(c2Field);

			JOptionPane.showMessageDialog(null,
					"Saisissez les numéros indiqués sur le plateau.");
			int resultat1 = JOptionPane.showConfirmDialog(null, case1,
					"S'il vous plait d'entrer la ligne et la colonne des valeurs de la 1ere case.",
					JOptionPane.OK_CANCEL_OPTION);
			if (resultat1 == JOptionPane.OK_OPTION) {
				l1 = Integer.parseInt(l1Field.getText());
				c1 = Integer.parseInt(c1Field.getText());
			}

			int resultat2 = JOptionPane.showConfirmDialog(null, case2,
					"S'il vous plait d'entrer la ligne et la colonne des valeurs de la 2eme case.",
					JOptionPane.OK_CANCEL_OPTION);

			if ((l1 <1 || l1 > plateau_.getnbCase()) || (c1 <1 || c1 > plateau_.getnbCase()))
			{
				JOptionPane.showMessageDialog(null,
						"Saisissez les numéros indiqués sur le plateau ! Recommencez");
			}
			Case Case1 = plateau_.getCase(l1-1, c1-1);
			if (resultat2 == JOptionPane.OK_OPTION) {
				l2 = Integer.parseInt(l2Field.getText());
				c2 = Integer.parseInt(c2Field.getText());
			}
			if ((l2 <1 || l2>plateau_.getnbCase()) || (c2 <1 || c2>plateau_.getnbCase()))
			{
				JOptionPane.showMessageDialog(null,
						"Saisissez les numéros indiqués sur le plateau ! Recommencez");
			}
			Case Case2 = plateau_.getCase(l2-1, c2-1);
			if (existeCheminCases(Case1, Case2)) {
				JOptionPane.showMessageDialog(null,
						"Oui, il existe un chemin entre les cases de la ligne = " + l1 + " et "
								+ " la colonne = " + c1 + " et la ligne = " + l2 + " et la colonne = " + c2 + " !");
			} else {
				JOptionPane.showMessageDialog(null,
						"Non, il n'existe pas le chemin entre les cases de la ligne= " + l1 + " et "
								+ " la colonne = " + c1 + " et la ligne = " + l2 + " et la colonne = " + c2 + " !");
			}
		} else if (e.getSource() == item8) {
			peutModif = 3;
			nombreEtoiles();
		} else if(e.getSource() == item9)
		{
			String message = afficheScores();
            JOptionPane.showMessageDialog(null,message);
		}
        else if(e.getSource() == item10)
        {
            int l1 = 0, c1 = 0;
            int l2 = 0, c2 = 0;

            JTextField l1Field = new JTextField(5);
            JTextField c1Field = new JTextField(5);
            JPanel case1 = new JPanel();
            case1.add(new JLabel("Ligne : "));
            case1.add(l1Field);
            case1.add(new JLabel("Colonne : "));
            case1.add(c1Field);

            JTextField l2Field = new JTextField(5);
            JTextField c2Field = new JTextField(5);
            JPanel case2 = new JPanel();
            case2.add(new JLabel("Ligne : "));
            case2.add(l2Field);
            case2.add(new JLabel("Colonne : "));
            case2.add(c2Field);

            JOptionPane.showMessageDialog(null,
                    "Saisissez les numéros indiqués sur le plateau.");
            int resultat1 = JOptionPane.showConfirmDialog(null, case1,
                   "S'il vous plait d'entrer la ligne et la colonne des valeurs de la 2eme case.",
                    JOptionPane.OK_CANCEL_OPTION);
            if (resultat1 == JOptionPane.OK_OPTION) {
                l1 = Integer.parseInt(l1Field.getText());
                c1 = Integer.parseInt(c1Field.getText());
            }

            int resultat2 = JOptionPane.showConfirmDialog(null, case2,
                    "S'il vous plait d'entrer la ligne et la colonne des valeurs de la 2eme case.",
                    JOptionPane.OK_CANCEL_OPTION);

            if ((l1 <1 || l1 > plateau_.getnbCase()) || (c1 <1 || c1 > plateau_.getnbCase()))
            {
                JOptionPane.showMessageDialog(null,
                        "Saisissez les numéros indiqués sur le plateau ! Recommence");
            }
            Case Case1 = plateau_.getCase(l1-1, c1-1);
            if (resultat2 == JOptionPane.OK_OPTION) {
                l2 = Integer.parseInt(l2Field.getText());
                c2 = Integer.parseInt(c2Field.getText());
            }
            if ((l2 <1 || l2>plateau_.getnbCase()) || (c2 <1 || c2>plateau_.getnbCase()))
            {
                JOptionPane.showMessageDialog(null,
                        "Saisissez les numéros indiqués sur le plateau ! Recommence");
            }
            Case Case2 = plateau_.getCase(l2-1, c2-1);
            Vector<Case> chemin = plateau_.relieCaseMin(Case1,Case2);
            String msg = "La distance entre " + Case1.toString() + " et "+ Case2.toString() + " est : " + (chemin.size()-1) +".\n";
            for (int cpt = 0; cpt < (chemin.size()-1);++cpt) msg+=chemin.get(cpt).toString();
            JOptionPane.showMessageDialog(null, msg);
        }
	}

	public static void main(String[] args) 
	{
		Main m = new Main();
		m.joueDeuxHumains();
	}
}