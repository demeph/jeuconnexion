import java.util.Vector;
class UC {
	private Vector<Pion> lesPions;

	public UC() {
		this.lesPions = new Vector<Pion>();
	}

	public void ajoute(Pion p) {
		this.lesPions.add(p);
        p.setIndiceVector(lesPions.size() - 1);
		p.setIndPere(lesPions.size() - 1); // affecte l'indice pion à l'indice du père
	}


	public boolean estConnecte(Pion p, Pion q) {
		return classe(p) == classe(q);
	}

	public int getSize() {
		return this.lesPions.size();
	}

	public int classe(Pion p) // indice du pion ds le vecteur
	{
		int ind = p.getIndiceVector();
		while (ind != lesPions.get(ind).getIndPere()) {
			ind = lesPions.get(ind).getIndPere();
		}
		return ind;
	}

	public void union(Pion p, Pion q) {
		int temp = 0, temp1 = 0;
		int racineP = classe(p);
		int racineQ = classe(q);

		if (racineQ == racineP) return;

		if (lesPions.get(racineP).getTailleComponente() < lesPions.get(racineQ).getTailleComponente()) {
			lesPions.get(racineP).setIndPere(racineQ);
			temp = lesPions.get(racineQ).getTailleComponente();
			temp1 = lesPions.get(racineP).getTailleComponente();
			lesPions.get(racineQ).setTailleComponente(temp + temp1);
		} else {
			lesPions.get(racineQ).setIndPere(racineP);
			temp = lesPions.get(racineQ).getTailleComponente();
			temp1 = lesPions.get(racineP).getTailleComponente();
			lesPions.get(racineP).setTailleComponente(temp + temp1);
		}
	}

	public Vector<Pion> getVector() {
		return this.lesPions;
	}

	public String toChaine(int i) {
		return lesPions.get(i).toString();
	}

}
