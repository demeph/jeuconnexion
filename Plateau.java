import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial")
class Plateau extends JPanel
{ // Classe personnelle qui crée une grille avec les carres.
	private Case lesCases[][];
	private Color col = Color.lightGray;
	private Color col1 = Color.black;
	private final static int cote = 35; // Ceci définit la taille du côté d'un polygone
	private static Image img;
	private Graphics2D g2d;
	private int numero = 0; // Retient le n° du polygone sur lequel est la souris
	private int nbCase = 11;
	private Polygon carre = getCarre(nbCase);
	private Polygon pol;
	private Random nbAleat;
	private Vector<Pion> lesEtoiles; // les etoiles qui se trouvent sur le plateau
	private Polygon p2;
	private Rectangle r;
	private boolean  reussi;
	private Vector<Case> openList;
	private Vector<Case> closedList;
	private int nbcaseTotal;


	public Plateau()
	{
		p2 = getPolygon(0, 0, cote); // Crée des carres
		r = p2.getBounds();
		try {
			loadImg();
		}catch (IOException e) {

		}
		reussi = false;
		//graph = (Graphics2D) this.getGraphics();
		lesEtoiles = new Vector<>();
		this.lesCases = new Case[nbCase][nbCase];
		for (int  l = 0; l < nbCase; ++l)
		{
			for (int c = 0; c < nbCase; ++c)
			{
				this.lesCases[l][c] = new Case(col,img,l,c);
			}
		}
		nbcaseTotal = nbCase*nbCase;
		nbAleat = new Random();
	}

	public static void loadImg() throws IOException
	{
		img = ImageIO.read(new File("star.png"));
	}

	public void  genererEtoiles(Joueur joueur,int nbEtoile)
	{
		for (int i = 0; i < nbEtoile; ++i) {
			// genere nombre aleatoire entre 0 et nbCase-1
			int nbAleat1 = nbAleat.nextInt(nbCase - 1);

			int nbAleat2 = nbAleat.nextInt(nbCase - 1);
			//ici on verifie que la case qu'on a selectioner est libre
			//si c'est pas libre on genere a nouveau nbAleat1 et nbAleat2
			while (lesCases[nbAleat1][nbAleat2].getPion() != null)
			{
				nbAleat1 = nbAleat.nextInt(nbCase - 1);
				nbAleat2 = nbAleat.nextInt(nbCase - 1);
			}
			joueur.ajouterEtoiles(lesCases[nbAleat1][nbAleat2]);
			lesCases[nbAleat1][nbAleat2].setAfficherEtoile(true);
			Pion p = new Pion(nbAleat1, nbAleat2, joueur.getColor());
			ajouterPion(joueur,p);
			lesEtoiles.add(p);
		}
	}

	@Override
	public void paint(Graphics arg0)
	{
		Point hovered = null;
		arg0.setColor(Color.black);
		super.paint(arg0);
		g2d = (Graphics2D) arg0;
		// Permet de fixer l'épaisseur du trait dans la suite
		BasicStroke bs3 = new BasicStroke(3);

        // Permet d'afficher numero des lignes et colonnes sur le plateau
        int cpt1 = 0;
        for (int ctp = 0; ctp < nbCase; ++ctp)
        {
            cpt1 = ctp+1;
            g2d.drawString(""+cpt1, 32+r.width*ctp, 12);
            g2d.drawString(""+cpt1, 3, 33+r.width*ctp);
        }

		//on recoupere la position actuel du souris
		Point q = getMousePosition();

		for(int l = 0;l < nbCase; ++l)
		{
			for(int c = 0;c < nbCase; ++c) {
				Polygon poly = getPolygon(c * r.height, l * r.width, cote);
				if (q != null && poly.contains(q)) {
					hovered = new Point(c * r.width, l * r.height);
					numero = l * nbCase + c;
					pol = poly;
				}
				//permet definir les bordures en noir
				g2d.setColor(Color.black);
				// determine l'epaisseur
				g2d.setStroke(bs3);
				//dessine le carre
				g2d.draw(poly);
				//recupere la couleur d'un case en fonction de matrice
				g2d.setColor(lesCases[l][c].getColor());
				//permet dessiner la case colorier
				g2d.fillPolygon(poly);
				//met l'image dans la carre correspondante

			}
		}

		for (int i = 0; i < lesEtoiles.size();++i)
		{
			int l = lesEtoiles.get(i).getPosL();
			int c = lesEtoiles.get(i).getPosC();
			g2d.drawImage(lesCases[l][c].getImg_(), c * r.height + 22, l * r.width + 17, 30, 30, null);
		}

		if(hovered!=null)
		{
			arg0.setColor(col1);
			g2d.setStroke(bs3);
			if ((hovered.x >= 0) && (hovered.x <= 350) && (hovered.y>=0) && (hovered.y<=350) )
			{
				Polygon p = getPolygon(hovered.x, hovered.y,cote);
				g2d.draw(p);
			}
		}
	}

	/**
	 * Permet obtenir les voisins d'une case qu'on passe en parametre
	 * @param p
	 * @return le vecteur de pion qui contient les voisins de p
     */
	public Vector<Pion> getVoisins(Pion p)
	{
		Vector<Pion>  lesVoisins= new Vector<Pion>();
		int l = p.getPosL(); int c = p.getPosC();
		if ( (( l > 0) && ( l < nbCase-1)) && ((c > 0) && ( c < nbCase-1)) ) {
			//les cases qui se touve a l'interiur des 4 bords
			lesVoisins.add(lesCases[l-1][c].getPion()); lesVoisins.add(lesCases[l-1][c+1].getPion());
			lesVoisins.add(lesCases[l][c+1].getPion()); lesVoisins.add(lesCases[l+1][c].getPion());
			lesVoisins.add(lesCases[l+1][c-1].getPion()); lesVoisins.add(lesCases[l][c-1].getPion());
			lesVoisins.add(lesCases[l+1][c+1].getPion());lesVoisins.add(lesCases[l-1][c-1].getPion());
		} else if ( ( c == 0) && ( l == 0) ) {
			//la premiere case du plateau
			lesVoisins.add(lesCases[l][c+1].getPion()); lesVoisins.add(lesCases[l+1][c].getPion());
			lesVoisins.add(lesCases[l+1][c+1].getPion());
		} else if (( l == 0 ) && (c > 0 ) && (c < (nbCase-1) ) ) {
			// les cases qui se trouve sur la premiere ligne entre 1ere et derniere case
			lesVoisins.add(lesCases[l][c+1].getPion()); lesVoisins.add(lesCases[l+1][c].getPion());
			lesVoisins.add(lesCases[l+1][c-1].getPion()); lesVoisins.add(lesCases[l][c-1].getPion());
			lesVoisins.add(lesCases[l+1][c+1].getPion());
		} else if  (( l > 0) && ( l < nbCase-1 ) && ( c == 0)) {
			//les cases qui se trouve sur la premiere colonne entre 1ere et derniere case
			lesVoisins.add(lesCases[l-1][c].getPion()); lesVoisins.add(lesCases[l-1][c+1].getPion());
			lesVoisins.add(lesCases[l][c+1].getPion()); lesVoisins.add(lesCases[l+1][c].getPion());
			lesVoisins.add(lesCases[l+1][c+1].getPion());
		} else if (( l == 0) && (c == nbCase-1) ) {
			//la case en haut et à droite
			lesVoisins.add(lesCases[l][c-1].getPion()); lesVoisins.add(lesCases[l+1][c-1].getPion());
			lesVoisins.add(lesCases[l+1][c].getPion()); 
		} else if (( l == nbCase-1) && (c == 0) ) {
			//la case en bas et à gauche
			lesVoisins.add(lesCases[l-1][c].getPion()); lesVoisins.add(lesCases[l-1][c+1].getPion());
			lesVoisins.add(lesCases[l][c+1].getPion());
		} else if ( ( l == nbCase-1) && ( c == nbCase-1) ) {
			//la case en bas et à droite
			lesVoisins.add(lesCases[l][c-1].getPion()); lesVoisins.add(lesCases[l-1][c].getPion());
			lesVoisins.add(lesCases[l-1][c-1].getPion());
		} else if ( (c == nbCase-1) && (l< nbCase-1) && (l>0)) {
			//les cases de la derniere colonne sauf premiere et la derniere case
			lesVoisins.add(lesCases[l-1][c].getPion()); lesVoisins.add(lesCases[l+1][c].getPion());
			lesVoisins.add(lesCases[l+1][c-1].getPion()); lesVoisins.add(lesCases[l][c-1].getPion());
		} else if ( (l == nbCase-1) && (c > 0) && (c < nbCase-1)) {
			// les cases de la derniere ligne sauf premiere et la derniere case
			lesVoisins.add(lesCases[l][c-1].getPion()); lesVoisins.add(lesCases[l-1][c].getPion());
			lesVoisins.add(lesCases[l-1][c+1].getPion()); lesVoisins.add(lesCases[l][c+1].getPion());
			lesVoisins.add(lesCases[l-1][c-1].getPion());
		}
		return lesVoisins;
	}

    /**
     * Permet obtenir les voisins d'une case qu'on passe en parametre
     * @param p
     * @return le vecteur de pion qui contient les voisins de p
     */
    public Vector<Case> getVoisinsCase(Case p)
    {
        Vector<Case>  lesVoisins= new Vector<Case>();
        int l = p.getLigne(); int c = p.getColonne();
        if ( (( l > 0) && ( l < nbCase-1)) && ((c > 0) && ( c < nbCase-1)) ) {
            //les cases qui se touve a l'interiur des 4 bords
            lesVoisins.add(lesCases[l-1][c]); lesVoisins.add(lesCases[l-1][c+1]);
            lesVoisins.add(lesCases[l][c+1]); lesVoisins.add(lesCases[l+1][c]);
            lesVoisins.add(lesCases[l+1][c-1]); lesVoisins.add(lesCases[l][c-1]);
            lesVoisins.add(lesCases[l+1][c+1]);lesVoisins.add(lesCases[l-1][c-1]);
        } else if ( ( c == 0) && ( l == 0) ) {
            //la premiere case du plateau
            lesVoisins.add(lesCases[l][c+1]); lesVoisins.add(lesCases[l+1][c]);
            lesVoisins.add(lesCases[l+1][c+1]);
        } else if (( l == 0 ) && (c > 0 ) && (c < (nbCase-1) ) ) {
            // les cases qui se trouve sur la premiere ligne entre 1ere et derniere case
            lesVoisins.add(lesCases[l][c+1]); lesVoisins.add(lesCases[l+1][c]);
            lesVoisins.add(lesCases[l+1][c-1]); lesVoisins.add(lesCases[l][c-1]);
            lesVoisins.add(lesCases[l+1][c+1]);
        } else if  (( l > 0) && ( l < nbCase-1 ) && ( c == 0)) {
            //les cases qui se trouve sur la premiere colonne entre 1ere et derniere case
            lesVoisins.add(lesCases[l-1][c]); lesVoisins.add(lesCases[l-1][c+1]);
            lesVoisins.add(lesCases[l][c+1]); lesVoisins.add(lesCases[l+1][c]);
            lesVoisins.add(lesCases[l+1][c+1]);
        } else if (( l == 0) && (c == nbCase-1) ) {
            //la case en haut et à droite
            lesVoisins.add(lesCases[l][c-1]); lesVoisins.add(lesCases[l+1][c-1]);
            lesVoisins.add(lesCases[l+1][c]);
        } else if (( l == nbCase-1) && (c == 0) ) {
            //la case en bas et à gauche
            lesVoisins.add(lesCases[l-1][c]); lesVoisins.add(lesCases[l-1][c+1]);
            lesVoisins.add(lesCases[l][c+1]);
        } else if ( ( l == nbCase-1) && ( c == nbCase-1) ) {
            //la case en bas et à droite
            lesVoisins.add(lesCases[l][c-1]); lesVoisins.add(lesCases[l-1][c]);
            lesVoisins.add(lesCases[l-1][c-1]);
        } else if ( (c == nbCase-1) && (l< nbCase-1) && (l>0)) {
            //les cases de la derniere colonne sauf premiere et la derniere case
            lesVoisins.add(lesCases[l-1][c]); lesVoisins.add(lesCases[l+1][c]);
            lesVoisins.add(lesCases[l+1][c-1]); lesVoisins.add(lesCases[l][c-1]);
        } else if ( (l == nbCase-1) && (c > 0) && (c < nbCase-1)) {
            // les cases de la derniere ligne sauf premiere et la derniere case
            lesVoisins.add(lesCases[l][c-1]); lesVoisins.add(lesCases[l-1][c]);
            lesVoisins.add(lesCases[l-1][c+1]); lesVoisins.add(lesCases[l][c+1]);
            lesVoisins.add(lesCases[l-1][c-1]);
        }
        return lesVoisins;
    }

	public Polygon getPol() { return pol; }

	public Polygon getCarre() { return carre; }

	public int getNumero() {return  numero;}

	public int getnbCase() {return  nbCase;}


	/**
	 * On affiche le composante d'un pion p pour player
	 * @param player
	 * @param p
     * @return
     */
	public String afficherComposante(Joueur player,Pion p)
	{
		Vector<Pion> lesPions = player.getVector();
		String afficher = "";
		afficher = "Les composantes du pion [ " + p.toString() + " ] sont :\n";
		for ( int cpt = 0; cpt < lesPions.size(); ++cpt)
		{
			if ((player.getUC().estConnecte(p,lesPions.get(cpt)))
					&& (!(p.sontEgaux(lesPions.get(cpt)))))
			{
				afficher += "- "+lesPions.get(cpt)+'\n';
			}
		}
		return afficher;
	}

	public Pion getPion(int l, int c)
	{
		return lesCases[l][c].getPion();
	}
	
	public Case getCase(int l, int c){
		return lesCases[l][c];
	}
	
	public boolean estDispo(int l, int c)
	{
		return (lesCases[l][c].getPion() == null ) ? true : false;
	}

    public void colorerCase(Pion p)
    {
        int l = p.getPosL(); int c = p.getPosC();
        lesCases[l][c].setColor(p.getColor());
        lesCases[l][c].setPion(p);
        repaint();
    }
	
	// qui permet de relier entre les joueurs et les pions
	public void reliePion(Joueur player,Pion p)
	{
		Color couleurp = p.getColor();
		Vector<Pion> lesVoisins = getVoisins(p);
		for(int i = 0;i < lesVoisins.size();++i )
		{
            Pion q = lesVoisins.get(i);
            if ((q != null) && (q.getColor() == couleurp))
			{
//				if (!(player.getUC().estConnecte(p, q)))
//				{
					player.getUC().union(p, q);
//				}
			}
		}
	}

	//Ajouter des pions
	public void ajouterPion(Joueur player,Pion p)
	{
		colorerCase(p);
		player.ajoutePion(p);
		reliePion(player,p);
	}
	

	// Verifie si relieComposante est vraie ou fausse
	boolean relieComposantes(Joueur j,Pion p)
	{
		Vector<Pion> lesVoisins = getVoisins(p);
		boolean connecte = false;
		int i =0;
		Vector<Pion> memePions = new Vector<Pion>();
        //on garde les voisins de meme couleur que p
		for (i=0;i < lesVoisins.size();++i)
		{
			if ((lesVoisins.get(i)!=null) && (p.getColor() == lesVoisins.get(i).getColor())) {
                memePions.add(lesVoisins.get(i));
            }
		}
		if ((memePions.size() == 0) || (memePions.size() ==1)) { 
			return connecte;
		}
		else
		{
			connecte = true;
		}
		return connecte;
	}

	
	public void reinitialiser()
	{
		Color clr = Color.lightGray;
		for (short l= 0; l< nbCase; l++)
		{
			for (short c = 0; c < nbCase; c++){
				lesCases[l][c].setColor(clr);
				lesCases[l][c].setPion(null);
				lesCases[l][c].setAfficherEtoile(false);
			}
		}
		nbcaseTotal = nbCase*nbCase;
		lesEtoiles.removeAllElements();
	}

    //Ceci qui permet de calculer la 1ere case selectionnée vers a la derniere case qu'on veut y aller
	public Vector<Case> relieCaseMin(Case depart,Case arrivee) {
		// TODO check input
		Color couleurDepart = depart.getPion().getColor();
		openList = new Vector<Case>();
		closedList = new Vector<Case>();
		openList.add(depart); 


		Case current;
		while (!reussi) {
			current = lowestFInOpen(); 
			closedList.add(current); 
			openList.remove(current); 

			if ((current.getColonne() == arrivee.getColonne())
					&& (current.getLigne() == arrivee.getLigne())) {
				return calcPath(depart, current);
			}
			Vector<Case> voisins = getVoisinsCase(current);
			Vector<Case> lesvoisinsDispo = new Vector<Case>();
			for (int cpt = 0; cpt< voisins.size();++cpt)
			{
				int c = voisins.get(cpt).getColonne();
				int l = voisins.get(cpt).getLigne();
				if ((lesCases[l][c].caseVide() || (couleurDepart == lesCases[l][c].getColor())) && (!(closedList.contains(voisins.get(cpt)))))
				{
					lesvoisinsDispo.add(voisins.get(cpt));
				}
			}
			for (int i = 0; i < lesvoisinsDispo.size(); i++) {
				Case currentAdj = lesvoisinsDispo.get(i);
				if (!openList.contains(currentAdj)) { 
					currentAdj.setPrecedent(current); 
					currentAdj.setCoutHauteur(arrivee);
					currentAdj.setgCosts(current); 
					openList.add(currentAdj); 
				} else { 
					if (currentAdj.getgCosts() > currentAdj.calculategCosts(current)) { 
						currentAdj.setPrecedent(current);
						currentAdj.setgCosts(current); 
					}
				}
			}

			if (openList.isEmpty()) { 
				return new Vector<>();
			}
		}
		return null;
	}

	private Vector<Case> calcPath(Case start, Case goal) {
		// TODO if invalid nodes are given (eg cannot find from
		// goal to start, this method will result in an infinite loop!)
		Vector<Case> path = new Vector<Case>();

		Case curr = goal;
		boolean done = false;
		while (!done) {
			path.add(0,curr);
			curr = (Case) curr.getPrecedent();

			if (curr.equals(start)) {
				done = true;
			}
		}
		return path;
	}

	private Case lowestFInOpen() {
		// TODO currently, this is done by going through the whole openList!
		Case cheapest = openList.get(0);
		for (int i = 0; i < openList.size(); i++) {
			if (openList.get(i).getfCosts() < cheapest.getfCosts()) {
				cheapest = openList.get(i);
			}
		}
		return cheapest;
	}

    public int getNbcaseTotal() {
		return nbcaseTotal;
	}

	public void setNbcaseTotal(int nbcaseTotal) {
		this.nbcaseTotal = nbcaseTotal;
	}


	//----------Partie Graphique----------
	public static Polygon getPolygon(int x,int y,int cote){// Forme le polygon
		Polygon p = new Polygon();
        y +=15;
        x +=20;
        //on dessine en suivan les points definit dans l'ordre
        p.addPoint(x,y+cote); //le point en bas a gauche
		p.addPoint(x,y); // le point en haut et a gauche
		p.addPoint(x+cote,y); // le point en haut a droite
		p.addPoint(x+cote,y+cote); // le point en bas a droite
		return p;
	}

	public static Polygon getCarre(int nbCase)
	{
		Polygon p2=getPolygon(0, 0, cote); // Crée un hexagone
		Rectangle r=p2.getBounds(); 
		Polygon carre = new Polygon();
		//le point en bas a gauche	
		carre.addPoint(20,15+nbCase*cote);
		// le point en haut et a gauche
		carre.addPoint(20,15);
		// le point en haut a droite
		carre.addPoint(20+nbCase*cote,35);
		// le point en bas a droite
		carre.addPoint(20+nbCase*cote,15+nbCase*cote);
		
		return carre;
	}
}