import java.awt.Color;
import java.util.Vector;

class Joueur
{
	private String nom ;
	private Color couleur;
	private UC thePions;
	private Vector<Case> lesEtoiles;
	
	public Joueur(String nom,Color col)
	{
		this.nom = nom;
		this.couleur = col;
		this.thePions = new UC();
		lesEtoiles = new Vector<Case>();
	}
	
	public void ajoutePion(Pion p)
	{
		this.thePions.ajoute(p);
	}
	
	public String getNom()
	{
		return this.nom;
	}

	public int getTaille()
	{
		return thePions.getSize();
	}

	public Color getColor()
	{
		return this.couleur;
	}
	
	public void setColor(Color color)
	{
		this.couleur = color;
	}

	public void ajouterEtoiles(Case case_) {lesEtoiles.add(case_);	}

	/**
	 * Recupere le vecteur des pions que joueur a ajoute
	 * @return
     */
	public Vector<Pion> getVector()
	{
		return thePions.getVector();
	}

	public int nombreEtoiles(){ return lesEtoiles.size(); }

	public Vector<Case> getEtoiles() { return lesEtoiles;	}

	@Override
	public String toString() {
		return nom;
	}

	public UC getUC()
	{
		return thePions;
	}
	
	public String toChaine(int i)
	{
		return thePions.toChaine(i-1);
	}
}