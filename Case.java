import java.awt.*;

class Case
{
    int ligne;
    int colonne;
    private Color couleur;
    private Pion pion;
    private Image img_;
    private boolean afficherEtoile;
    private Case precedent;
    private int coutHauteur;
    private int gCosts;


    public Case(Color couleur, Image img, int l, int c)
    {

        this.couleur = couleur;
        ligne = l;
        colonne = c;
        img_ = img;
        this.pion = null;
        afficherEtoile = true;
    }

    public Color getColor()
    {
        return this.couleur;
    }

    public int getLigne() {
        return ligne;
    }

    public int getColonne() {
        return colonne;
    }

    public void setColor(Color col)
    {
        this.couleur = col;
    }

    public boolean caseVide(){
        return pion == null;
    }

    public Pion getPion()
    {
        return this.pion;
    }

    public boolean equals(Case o) {
        if (getLigne() != o.getLigne()) return false;
        return getColonne() == o.getColonne();

    }

    public Image getImg_() { return img_;}

    public boolean getAfficherEtoile() {
        return afficherEtoile;
    }

    public void setAfficherEtoile(boolean val) { afficherEtoile = val;}

    public void setPion(Pion p )
    {
        this.pion  = p;
    }

    public void setPrecedent(Case precedent) {
        this.precedent = precedent;
    }

    public void setCoutHauteur(Case endNode) {
        this.coutHauteur=((Math.abs(this.getColonne() - endNode.getColonne())
                + Math.abs(this.getLigne()) - endNode.getLigne())* 10);
    }

    @Override
    public String toString() {
        return "Case{" +
                "ligne=" + (ligne+1)+
                ", colonne=" + (colonne+1) +
                "}\n";
    }

    public void setgCosts(Case c)
    {
        setgCosts(c.getgCosts()+10);
    }

    private void setgCosts(int i) {
        this.gCosts = i;
    }

    public int getgCosts() {
        return gCosts;
    }

    public Case getPrecedent() {
        return precedent;
    }

    public int calculategCosts(Case c)
    {
        return (precedent.getgCosts()+10);
    }

    public int getfCosts()
    {
        return  gCosts+coutHauteur;
    }
}

