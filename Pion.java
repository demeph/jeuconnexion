import java.awt.Color;

class Pion
{
    private int posC;
    private int posL;
    private int indPere;
    private int tailleComponente; // la taille de l'arbre
    private int indiceVector;
    private Color color;

    /**
     * constructeur de pion,
     * met le representant de case a -1 modifie plus tard par un fonction ajoutePion de la class UC
     * @param l numero de ligne sur le plateau [0;nbCase-1]
     * @param cl numero de colonne sur le plateau [0;nbCase-1]
     * @param c Couleur du pion, determiné en fonction de joueur qui joue
     */
    public Pion(int l, int cl, Color c)
    {
        this.posL = l;
        this.posC = cl;
        this.color = c;
        this.tailleComponente = 1;
        this.indPere = -1;
    }

    /**
     * getters de la ligne courante de pion
     * @return la position courante de la ligne de pion
     */
    public int getPosL()
    {
        return this.posL;
    }

    /**
     * getters de la colonne courante de pion
     * @return la position courante de la colonne de pion
     */
    public int getPosC()
    {
        return this.posC;
    }

    /**
     * permet de recuperer le representat de la classe que pion fait partie
     * @return
     */
    public int getIndPere()
    {
        return this.indPere;
    }


    /**
     * verifie si this et p sont meme pions
     * @param p pion
     * @return vrai si même pion, sinon faux
     */
    public boolean sontEgaux(Pion p)
    {
        if (this.getPosC() == p.getPosC() && (this.getPosL() == p.getPosL()) && this.color == p.getColor())
            return true;
        else
            return false;
    }

    /**
     * setter qui modifie le representant de la classe
     * @param i la nouvelle representant de la classe
     */
    public void setIndPere(int i)
    {
        this.indPere = i;
    }

    /**
     * recupere la couleur de pion
     * @return
     */
    public Color getColor()
    {
        return this.color;
    }


    public int getTailleComponente() {
        return tailleComponente;
    }

    public void setTailleComponente(int tailleComponente) {
        this.tailleComponente = tailleComponente;
    }

    /**
     * Fonction to string, recapitulatif du pion
     * @return
     */
    public String toString()
    {
        String str = " ligne : " + (posL+1) +", colonne : " + (posC+1) + ", indicePere : " + indPere;
        str += ", taille composante : " + tailleComponente;
        return str;
    }

    public int getIndiceVector() {
        return indiceVector;
    }

    public void setIndiceVector(int indiceVector) {
        this.indiceVector = indiceVector;
    }
}